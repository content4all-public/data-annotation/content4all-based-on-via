# Pack self contained CONTENT4ALL based on VIA application
#
# Author: Marco Giovanelli for FINCONS GROUP AG within the CONTENT4ALL Project
# Credits: Abhishek Dutta <adutta@robots.ox.ac.uk>
# Date: 13 July 2020

import string
import os
import sys
import csv
import shutil
import fileinput
import io

DIST_PACK_DIR = os.path.dirname(os.path.realpath(__file__))
VIA_SRC_DIR = os.path.join(DIST_PACK_DIR, '..')
if len(sys.argv) != 2:
  print("Usage: python3 pack_content4all_based_on_via.py version")
  print("e.g.: python3 pack_content4all_based_on_via.py 1.0.0")
  sys.exit()

VERSION = sys.argv[1]
TARGET_HTML = os.path.join(VIA_SRC_DIR, 'src', 'html', '_via_video_annotator.html')
DIST_DIR = os.path.join(VIA_SRC_DIR, 'dist')
if not os.path.exists(DIST_DIR):
    os.mkdir(DIST_DIR)

def get_src_file_contents(filename):
  full_filename = os.path.join(VIA_SRC_DIR, 'src', filename)
  with open(full_filename) as f:
    return f.read()

# Pack files in a virtual "source" file
source = io.StringIO()
with open(TARGET_HTML, 'r') as inf:
  for line in inf:
    if '<script src="' in line:
      tok = line.split('"')
      filename = tok[1][3:]
      source.write('<!-- START: Contents of file: ' + filename + '-->\n')
      source.write('<script>\n')
      source.write( get_src_file_contents(filename) )
      source.write('</script>\n')
      source.write('<!-- END: Contents of file: ' + filename + '-->\n')
    else:
      if '<link rel="stylesheet" type="text/css"' in line:
        tok = line.split('"')
        filename = tok[5][3:]
        source.write('<!-- START: Contents of file: ' + filename + '-->\n')
        source.write('<style>\n')
        source.write( get_src_file_contents(filename) )
        source.write('</style>\n')
        source.write('<!-- END: Contents of file: ' + filename + '-->\n')
      else:
        parsedline = line
        if "//__ENABLED_BY_PACK_SCRIPT__" in line:
          parsedline = line.replace('//__ENABLED_BY_PACK_SCRIPT__', '');

        source.write(parsedline)

# Translate "source" and save it in a file for each language
with open("multilanguage.csv", encoding='utf-8-sig', mode='r') as csv_file:
  input_csv = csv.DictReader(csv_file)
  for lang in input_csv.fieldnames[1:]:

    # Replace placeholders with translations
    out_temp = io.StringIO()
    source.seek(0)
    for line in source:
      line = line.replace('__CONTENT4ALL_BASED_ON_VIA_VERSION__', VERSION)
      if '__ML' in line:
        csv_file.seek(0)
        is_header = True
        for row in input_csv:
          if not is_header:
            line = line.replace(row["string_id"], row[lang])
          is_header = False
      out_temp.write(line)

    # Save to file
    out_path = os.path.join(DIST_DIR, 'content4all_based_on_via_v' + VERSION + '_' + lang + '.html')
    with open (out_path, encoding='utf-8-sig', mode='w') as out_file:
      out_temp.seek(0)
      shutil.copyfileobj(out_temp, out_file)
    print("Written packed file to: " + out_path)
