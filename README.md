# CONTENT4ALL based on VIA

CONTENT4ALL based on VIA is a tool to align videos transcriptions to the relative Sign Language interpretations for the [CONTENT4ALL Project](http://content4all-project.eu/) (an European Union’s Horizon 2020 research and innovation funded project).  
The goal is to obtain ground-truth data for the development of the CONTENT4ALL automatic translator of spoken/written language to Sign Language.  
The tool is based on the [VGG Image Annotator (VIA)](http://www.robots.ox.ac.uk/~vgg/software/via) version [3.0.7](https://gitlab.com/vgg/via/-/tree/via-3.0.7) of Abhishek Dutta (Visual Geometry Group of Oxford University).


## Getting Started

### Prerequisites

* Python 3.7 (or greater)
* HTML5-compatible web-browser
* A `.json` Project file (see [Subtitles to VIA Converter](https://gitlab.com/content4all-public/data-annotation/subtitles-to-via-converter))

### Build

Launch `scripts\pack_content4all_based_on_via.py` with `x.y.z` as argument.  
It will be created one packed html files for each language (`dist\content4all_based_on_via_v***.html`).

### Usage

1. Open `content4all_based_on_via_v***.html` with a web-browser
2. Select the `folder icon` button on the top and browse to the `.json` Project file
3. Follow the on-screen guide or look at the shortcuts list (`keyboard icon` button on the top)
4. Save the work with the `floppy icon` button on the top
    > N.B. By default most of web-browsers save it in a predefined download folder.  
    To change the destination folder please refer to your web-browser guide.  

### Multilanguage Support

The currently supported languages are:
* English
* Swiss-German

In order to add support other languages, `scripts/multilanguage.csv` is provided (encoding: `UTF8 with BOM`).  
It can be edited with Microsoft Excel (or equivalent).  
  
The file is structured as follow:
* One column for each language, identified by alpha-3 code (see ISO 639-2)
    > E.G. eng (English), gsw (Swiss-German), dut (Flemish), ...
* Each row is uniquely identified by the `string_id`.  
  The `string_id` is composed by `ML` followed by four digit incremental number, a short description in capital letters and is delimited by two `__`.  
  > E.G. `__ML1234_SHORT_DESCRIPTION__`

Each `string_id` found in the source files (e.g. `_via_video_annotator.html`, ...) will be replaced by the corresponding string in the `multilanguage.csv` for each language.

### Development

This software was developed with Visual Studio Code on Windows 10.  
The `.vscode/tasks.json` (build) and `.vscode/launch.json` (debug) are provided for convenience.  
For further information about the code and the `.json` Project file, please refer to the original documentation [CodeDoc.md](CodeDoc.md).
  > N.B. The `flg` attribute of the `.json` Project file is now used to store if a Temporal Segments is discarded (`flg = 1`).


## Authors

This software was developed by Marco Giovanelli for FINCONS GROUP AG within the CONTENT4ALL Project.  
For any further information, please send an email to [content4all-dev@finconsgroup.com](mailto:content4all-dev@finconsgroup.com).

## License
This software is released as Open Source according to the "2-Clause BSD License":

```
Copyright (c) 2020, developed by Marco Giovanelli for FINCONS GROUP AG
within the CONTENT4ALL Project.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Portions of this program were originally released under the following license:

Copyright (c) 2019, Abhishek Dutta, Visual Geometry Group, Oxford University
and VIA Contributors.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
```
